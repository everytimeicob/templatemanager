<?php

/**
 * The purpose of this service is to matches placeholders for replacements.
 *
 * [quote:*]
 */
class QuoteTemplateService implements TemplateServiceContract
{
    /**
     * A Quote instance.
     *
     * @var Quote
     */
    private $quote;

    /**
     * A Site instance.
     *
     * @var Site
     */
    private $site;

    /**
     * A Destination instance.
     *
     * @var Destination
     */
    private $destination;


    /**
     * Constructor.
     *
     * @param  Quote $quote A Quote instance.
     *
     * @throws RuntimeException thrown if the relationships are invalid.
     *
     * @return void
     */
    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
        $this->site = SiteRepository::getInstance()->getById($this->quote->siteId);
        $this->destination = DestinationRepository::getInstance()->getById($this->quote->destinationId);

        if (!($this->site instanceof Site)) {
            throw new RuntimeException('The Site belonging to the current Quote is not a valid instance.');
        }

        if (!($this->destination instanceof Destination)) {
            throw new RuntimeException('The Destination belonging to the current Quote is not a valid instance.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function replaceContent($text)
    {
        if (strpos($text, '[quote:summary_html]') !== false) {
            $text = str_replace('[quote:summary_html]', $this->quote->getHtml(), $text);
        }

        if (strpos($text, '[quote:summary]') !== false) {
            $text = str_replace('[quote:summary]', $this->quote->getText(), $text);
        }

        if (strpos($text, '[quote:destination_name]') !== false) {
            $text = str_replace('[quote:destination_name]', $this->destination->countryName, $text);
        }

        return $text;
    }

    /**
     * Returns the quote.
     *
     * @return Quote
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * Returns the site.
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Returns the destination.
     *
     * @return Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }
}
