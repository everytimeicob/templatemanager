<?php

/**
 * The purpose of this service is to matches placeholders for replacements.
 *
 * [user:*]
 */
class UserTemplateService implements TemplateServiceContract
{
    /**
     * An User instance.
     *
     * @var User
     */
    private $user;


    /**
     * Constructor.
     *
     * @param  User $user An User instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function replaceContent($text)
    {
        if (strpos($text, '[user:first_name]') !== false) {
            $text = str_replace('[user:first_name]', ucfirst(mb_strtolower($this->user->firstname)), $text);
        }

        return $text;
    }
}
