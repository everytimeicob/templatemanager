<?php

interface TemplateServiceContract
{
    /**
     * Replaces all the placeholders with their matching contents.
     *
     * @param  string $text The text containing the placeholders.
     *
     * @throws RuntimeException Thrown if an error occures.
     *
     * @return string
     */
    public function replaceContent($text);
}
