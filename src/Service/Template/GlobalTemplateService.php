<?php

/**
 * The purpose of this service is to matches placeholders for replacements.
 */
class GlobalTemplateService implements TemplateServiceContract
{
    /**
     * A Site instance.
     *
     * @var Site
     */
    private $site;

    /**
     * A Quote instance.
     *
     * @var Quote
     */
    private $quote;

    /**
     * A Destination instance.
     *
     * @var Destination
     */
    private $destination;


    /**
     * Constructor.
     *
     * @param  Site|null        $site A Site instance.
     * @param  Quote|null       $quote A Quote instance.
     * @param  Destination|null $destination A Destination instance.
     *
     * @return void
     */
    public function __construct(Site $site = null, Quote $quote = null, Destination $destination = null)
    {
        $this->site = $site;
        $this->quote = $quote;
        $this->destination = $destination;
    }

    /**
     * {@inheritdoc}
     */
    public function replaceContent($text)
    {
        if (strpos($text, '[quote:destination_link]') !== false) {
            if (isset($this->destination, $this->site, $this->quote)) {
                $text = str_replace(
                    '[quote:destination_link]',
                    $this->site->url
                    .'/'.
                    $this->destination->countryName
                    .'/quote/'.
                    $this->quote->id,
                    $text
                );
            } else {
                $text = str_replace('[quote:destination_link]', '', $text);
            }
        }

        return $text;
    }
}
