<?php

class Quote
{
    public $id;
    public $siteId;
    public $destinationId;
    public $dateQuoted;

    public function __construct($id, $siteId, $destinationId, $dateQuoted)
    {
        $this->id = $id;
        $this->siteId = $siteId;
        $this->destinationId = $destinationId;
        $this->dateQuoted = $dateQuoted;
    }

    /**
     * Retrieves the HTML of the current instance.
     *
     * @return string
     */
    public function getHtml()
    {
        return static::renderHtml($this);
    }

    /**
     * Retrieves the text of the current instance.
     *
     * @return string
     */
    public function getText()
    {
        return static::renderText($this);
    }

    /**
     * Retrieves the HTML of the current instance.
     *
     * @return string
     */
    public static function renderHtml(Quote $quote)
    {
        return '<p>' . $quote->id . '</p>';
    }

    /**
     * Retrieves the text of the current instance.
     *
     * @return string
     */
    public static function renderText(Quote $quote)
    {
        return (string) $quote->id;
    }
}