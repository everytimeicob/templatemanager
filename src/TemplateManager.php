<?php

class TemplateManager
{
    /**
     * An instance of Template.
     *
     * @var Template
     */
    private $template;

    /**
     * The template's data.
     *
     * @var array
     */
    private $templateData;

    /**
     * An array of template's services.
     *
     * @var array
     */
    private $services = [
        'global' => null,
        'quote' => null,
        'user' => null,
    ];


    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    /**
     * Retrieves the fully generated template.
     * Does not modify the definition!
     *
     * @param  Template $tpl  A Template instance.
     * @param  array    $data An array of data.
     *
     * @throws InvalidArgumentException Thrown if the template is invalid or if the data are empty.
     *
     * @return string
     */
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new InvalidArgumentException('The first parameter must be an instance of \Template.');
        } elseif (empty($data)) {
            throw new InvalidArgumentException('The second parameter should not be empty.');
        }

        $this->template = clone $tpl;
        $this->templateData = $data;

        $this->registerServices();

        return $this->generateTemplate();
    }

    /**
     * Registers all the template's services.
     *
     * @return void
     */
    private function registerServices()
    {
        $site = $quote = $destination = null;

        if (!empty($this->templateData['quote']) && $this->templateData['quote'] instanceof Quote) {
            $this->services['quote'] = new QuoteTemplateService($this->templateData['quote']);

            $quote = $this->services['quote']->getQuote();
            $site = $this->services['quote']->getSite();
            $destination = $this->services['quote']->getDestination();
        }

        if (!empty($this->templateData['user']) && $this->templateData['user'] instanceof User) {
            $this->services['user'] = new UserTemplateService($this->templateData['user']);
        } else {
            $this->services['user'] = new UserTemplateService(ApplicationContext::getInstance()->getCurrentUser());
        }

        $this->services['global'] = new GlobalTemplateService($site, $quote, $destination);
    }

    /**
     * Generates the template with all the needed replacements.
     * 
     * @return string
     */
    private function generateTemplate()
    {
        $this->template->subject = $this->replaceContent($this->template->subject);
        $this->template->content = $this->replaceContent($this->template->content);

        return $this->template;
    }

    /**
     * Replaces all the placeholders with their matching contents.
     *
     * @param  string $text The text containing the placeholders.
     *
     * @return string
     */
    private function replaceContent($text)
    {
        foreach ($this->services as $serviceName => $service) {
            if ($service) {
                $text = $service->replaceContent($text);
            }
        }

        return $text;
    }
}
